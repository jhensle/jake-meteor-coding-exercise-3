/*
 * Add query methods like this:
 *  NPCharacterResponse.findPublic = function () {
 *    return NPCharacterResponse.find({is_public: true});
 *  }
 */
NPCharacterResponse.allow({
  insert: function (userId, doc) {
    return true;
  },

  update: function (userId, doc, fieldNames, modifier) {
    return true;
  },

  remove: function (userId, doc) {
    return true;
  }
});

NPCharacterResponse.deny({
  insert: function (userId, doc) {
    return false;
  },

  update: function (userId, doc, fieldNames, modifier) {
    return false;
  },

  remove: function (userId, doc) {
    return false;
  }
});